package color

import (
	"gitlab.com/mgsearch/colorizrr"
	"gitlab.com/mgsearch/colorizrr/standardColor"
)

// NormalizeColor is just simple wrap for calling color normalization used inside HTTP or gRPC handlers.
func NormalizeColor(normalizer *colorizrr.Colorizrr, text string) []standardColor.Color {
	return normalizer.Normalize(text)
}
