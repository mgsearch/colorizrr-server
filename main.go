package main

import (
	"os"
	"os/signal"
	"strings"
	"syscall"
	"time"

	"github.com/rs/zerolog"
	"gitlab.com/mgsearch/colorizrr"
	grpcServer "gitlab.com/mgsearch/colorizrr-server/grpc/server"
	httpServer "gitlab.com/mgsearch/colorizrr-server/http/server"
)

type apiConfig struct {
	httpAddress          string
	grpcAddress          string
	swaggerFilesFullPath string
}

// main initializes configuration (or uses default values) and launches HTTP and gRPC servers which are ready for roll.
func main() {
	// init logger and apiConfig from ENV (if not set, use default values)
	logger := createLogger()
	config := initConfig(logger)

	// first init normalizers
	normalizers := make(map[string]*colorizrr.Colorizrr)
	for languageCode := range colorizrr.GetAllowedLanguages() {
		normalizer, err := colorizrr.New(languageCode)
		if err != nil {
			panic(err)
		}
		normalizers[languageCode] = normalizer
	}

	// prepare graceful exit
	done := make(chan os.Signal, 1)
	signal.Notify(done, syscall.SIGTERM, syscall.SIGINT) // sigterm for e.g. k8s, sig interrupt for e.g. ctrl + c

	// init and start the HTTP server on the background (as we need both HTTP and gRPC servers running simultaneously)
	newHttpServer := httpServer.NewHTTPServer(config.httpAddress, config.swaggerFilesFullPath, normalizers, &logger)
	go func() {
		err := newHttpServer.Start()
		if err != nil {
			logger.Fatal().Msg("unable to start HTTP server")
			panic(err)
		}
	}()
	logger.Debug().Msg("HTTP server listening: " + config.httpAddress)

	// init and start the gRPC server on the background (as we need both HTTP and gRPC servers running simultaneously)
	newGrpcServer := grpcServer.NewGRPCServer(config.grpcAddress, normalizers, &logger)
	go func() {
		err := newGrpcServer.Start()
		if err != nil {
			logger.Fatal().Msg("unable to start gRPC server")
			panic(err)
		}
	}()
	logger.Debug().Msg("gRPC server listening: " + config.grpcAddress)

	// wait until OS signal channel is terminated
	<-done

	// always terminate gracefully without force killing active connections
	// terminate gRPC
	newGrpcServer.GracefulStop()
	logger.Debug().Msg("gRPC server terminated gracefully")

	// terminate HTTP
	err := newHttpServer.GracefulStop()
	if err != nil {
		logger.Fatal().Msg("unable to terminate gracefully HTTP server")
		panic(err)
	}
	logger.Debug().Msg("HTTP server terminated gracefully")
}

func createLogger() zerolog.Logger {
	zerolog.TimeFieldFormat = time.RFC3339
	logger := zerolog.New(os.Stdout).
		With().Timestamp().Logger().
		With().Str("app", "colorizrr-server").Logger()
	return logger
}

// initConfig just sets basic default configuration which can be overridden from ENV. As we have 2 parameters,
// there is no need to use complex configuration libraries at this time.
func initConfig(logger zerolog.Logger) apiConfig {
	httpAddress := os.Getenv("HTTP_ADDRESS")
	if strings.TrimSpace(httpAddress) == "" {
		httpAddress = ":11111"
	}
	grpcAddress := os.Getenv("GRPC_ADDRESS")
	if strings.TrimSpace(grpcAddress) == "" {
		grpcAddress = ":22222"
	}
	swaggerFilesFullPath := os.Getenv("SWAGGER_FILES_FULL_PATH")
	if strings.TrimSpace(swaggerFilesFullPath) == "" {
		swaggerFilesFullPath = "/colorizrr-server/http/swagger/"
	}

	return apiConfig{
		httpAddress:          httpAddress,
		grpcAddress:          grpcAddress,
		swaggerFilesFullPath: swaggerFilesFullPath,
	}
}
