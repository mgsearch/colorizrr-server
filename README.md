# Color Normalizer Server (HTTP/2 and gRPC)

#### by MALL Group Search Lab

Colorizrr Server is a stateless server providing **REST API** and **gRPC API** for **color normalization** using [https://gitlab.com/mgsearch/colorizrr](https://gitlab.com/mgsearch/colorizrr) - MALL Group Search Lab library color normalizer.

It tries to understand **input color(s)** (or basically input text) and **converts it into standardized, normalized colors**, e.g. `"RED / LIME / MARINE"` converts into
```json
{
    "colors": [
        {
            "id": "red",
            "name": "red",
            "root": "red",
            "rgb": "FF0000"
        },
        {
            "id": "green",
            "name": "green",
            "root": "green",
            "rgb": "008000"
        },
        {
            "id": "blue",
            "name": "blue",
            "root": "blue",
            "rgb": "0000FF"
        }
    ]
}
``` 

## Usage

#### HTTP/2

We provide **REST API at** [https://colornormalizer.api.mallsearchlab.cz](https://colornormalizer.api.mallsearchlab.cz)

**Request example**: `POST https://colornormalizer.api.mallsearchlab.cz/v1/normalize` with body
```json
{
  "text": "dark red / wine, mint",
  "language_code": "en"
}
```

**Response**: 
```json
{
    "colors": [
        {
            "id": "red",
            "name": "red",
            "root": "red",
            "rgb": "FF0000"
        },
        {
            "id": "green",
            "name": "green",
            "root": "green",
            "rgb": "008000"
        }
    ]
}
``` 

No credentials or security tokens needed.

#### gRPC

We provide **gRPC API at** [colornormalizer.grpc.mallsearchlab.cz](colornormalizer.grpc.mallsearchlab.cz) port 80

**Protocol contract**:

```protobuf
syntax = "proto3";

package proto.v1;

service Normalizer {
  rpc Normalize (Request) returns (Response) {}
}

message Request {
  string language_code = 1;
  string text = 2;
}

message Response {
  message Colors {
    string id = 1;
    string name = 2;
    string root = 3;
    string rgb = 4;
  }

  repeated Colors colors = 1;
}
```

No credentials or security tokens needed.

## Documentation

#### HTTP/2

See our [Swagger API documentation](https://colornormalizer.api.mallsearchlab.cz/v1/doc/index.html)

#### gRPC

See our [proto file](../grpc/v1/proto/normalize.proto)

Working example of your own gRPC client in Golang:
```go
package main

import (
	"context"
	"log"
	"time"

	"gitlab.com/mgsearch/colorizrr-server/grpc/v1/proto"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

func main() {
	// this is working example you can test right now

	// establish connection
	connection, err := grpc.Dial(
		"colornormalizer.grpc.mallsearchlab.cz:80",
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		panic(err)
	}
	defer connection.Close()

	// create gRPC client
	// this proto.NewNormalizerClient is generated from proto file by protobuf
	// see https://grpc.io/docs/languages/go/quickstart/
	// or go to our folder gitlab.com/mgsearch/colorizrr-server/grpc/v1/proto
	// and run `protoc --go_out=. --go_opt=paths=source_relative --go-grpc_out=. --go-grpc_opt=paths=source_relative normalize.proto`
	grpcClient := proto.NewNormalizerClient(connection)

	// and finally make your gRPC call
	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()
	response, err := grpcClient.Normalize(ctx, &proto.Request{
		LanguageCode: "en",
		Text:         "dark red / wine, mint",
	})
	if err != nil {
		panic(err)
	}

	// response:
	// colors:{id:"red"  name:"red"  root:"red"  rgb:"FF0000"}  colors:{id:"green"  name:"green"  root:"green"  rgb:"008000"}
	log.Println(response)
}
```

## Benchmarks - HTTP

For benchmarks [Bombardier](https://pkg.go.dev/github.com/codesenberg/bombardier) was used, test duration 10 sec., 125 concurrent connections.

#### Local network

1 pod, no CPU and memory limits, local network

| Statistics | Avg | Stdev | Max
| --- | --- | --- | ---
| Reqs. / sec | 72117.17 | 3812.37 | 83034.46
| Latency | 1.73 ms | 1.92 ms | 48.05 ms
  
HTTP codes: 1xx - 0, 2xx - 721078, 3xx - 0, 4xx - 0, 5xx - 0, others - 0, throughput: 31.21 MB/s

#### Reqeusts to colornormalizer.api.mallsearchlab.cz REST API 

3 pods, limits 500 mCPU and 500 Mi memory, network distance about 500 km (which makes the most of latency)

| Statistics | Avg | Stdev | Max
| --- | --- | --- | ---
| Reqs. / sec | 2762.11 | 1377.49 | 6567.72
| Latency | 45.31 ms | 16.88 ms | 284.18 ms

HTTP codes: 1xx - 0, 2xx - 27639, 3xx - 0, 4xx - 0, 5xx - 0, others - 0, throughput 803.39 KB/s

## Deployment 

#### Docker Image

If you want to run your local server e.g. as microservice (as it will be much, much faster than calling it to the other side of this planet via internet), you can simply use our docker images from our [docker registry](https://gitlab.com/mgsearch/colorizrr-server/container_registry/2730392) or just use our source:

`registry.gitlab.com/mgsearch/colorizrr-server:1.3.2`

#### Kubernetes

If you run Kubernetes cluster, you can deploy your own server with a snap of your finger. See our [k8s konfigs](../docker/k8s) and simply apply them (some minor tweaking should be necessary depending on your cluster configuration).
