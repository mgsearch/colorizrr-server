package server

import (
	"net/http"
	"strings"
	"time"

	"github.com/dgrr/http2"
	"github.com/rs/zerolog"
	"github.com/valyala/fasthttp"
	"gitlab.com/mgsearch/colorizrr"
	"gitlab.com/mgsearch/colorizrr-server/http/swagger"
	"gitlab.com/mgsearch/colorizrr-server/http/v1/normalize"
	"go.elastic.co/apm/module/apmfasthttp"
)

// HttpServer is a simple wrap around basic fasthttp server
type HttpServer struct {
	address string
	http    *fasthttp.Server
}

// NewHTTPServer creates HTTP/2 server running on provided address.
func NewHTTPServer(
	address string,
	swaggerFilesFullPath string,
	normalizers map[string]*colorizrr.Colorizrr,
	logger *zerolog.Logger,
) *HttpServer {
	server := &fasthttp.Server{
		Handler:     apmfasthttp.Wrap(buildRoutes(normalizers, swaggerFilesFullPath, logger)), // use APM plugin to be able to extract app metrics to APM if needed
		ReadTimeout: 5 * time.Second,                                                          // set timeouts to be able to exit gracefully
		IdleTimeout: 5 * time.Second,
	}
	// switch server to HTTP/2 for better performance
	http2.ConfigureServer(server)

	return &HttpServer{
		http:    server,
		address: address,
	}
}

func (server *HttpServer) Start() error {
	return server.http.ListenAndServe(server.address)
}

func (server *HttpServer) GracefulStop() error {
	return server.http.Shutdown()
}

// buildRoutes is just some basic router. As HTTP is more complicated than gRPC in the way of what can be called,
// how and what to say back etc., this contains several ifs but still this is quite straight forward
// and performance more friendly solution than "hey buddy, find some routing library on Stack Overflow" solution.
func buildRoutes(normalizers map[string]*colorizrr.Colorizrr, swaggerFilesFullPath string, logger *zerolog.Logger) func(ctx *fasthttp.RequestCtx) {
	return func(ctx *fasthttp.RequestCtx) {
		ctx.SetContentType("application/json")
		ctx.SetStatusCode(fasthttp.StatusOK)

		if string(ctx.Method()) == fasthttp.MethodOptions {
			// CORS for GUIS like Swag - this is enough, no additional "cors bundle" needed
			ctx.Response.Header.Set("Access-Control-Allow-Origin", "*")
			return
		} else if string(ctx.Path()) == "/v1/normalize" && string(ctx.Method()) == fasthttp.MethodPost {
			// route for normalizer
			normalize.NewNormalizeHandler().Normalize(normalizers, ctx)
		} else if strings.Contains(string(ctx.Path()), "/v1/doc/") && string(ctx.Method()) == fasthttp.MethodGet {
			// route for swagger
			swagger.NewSwaggerHandler().Swag(ctx, swaggerFilesFullPath)
		} else if string(ctx.Path()) == "/" && string(ctx.Method()) == fasthttp.MethodGet {
			// redirect from "homepage" to apidoc
			ctx.Redirect("/v1/doc/index.html", http.StatusMovedPermanently)
		} else {
			// all the rest
			ctx.SetStatusCode(fasthttp.StatusNotFound)
			ctx.SetBody([]byte(`{"error":"endpoint not found"}`))
		}

		logger.Debug().Str("protocol", "HTTP").Str("event", "request").Str("request_data", ctx.Request.String()).Send()
		logger.Debug().Str("protocol", "HTTP").Str("event", "response").Str("response_data", ctx.Response.String()).Send()
	}
}
