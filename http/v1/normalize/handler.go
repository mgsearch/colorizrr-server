package normalize

import (
	"encoding/json"
	"strings"

	"github.com/valyala/fasthttp"
	"gitlab.com/mgsearch/colorizrr"
	"gitlab.com/mgsearch/colorizrr-server/service/color"
)

// NewSwaggerHandler is just a constructor for NormalizeHandler
func NewNormalizeHandler() *NormalizeHandler {
	return &NormalizeHandler{}
}

// NormalizeHandler provides simple handler for handling color normalization.
type NormalizeHandler struct{}

type request struct {
	LanguageCode string `json:"language_code"`
	Text         string `json:"text"`
}

// Normalize is color normalization handler for HTTP,
func (handler *NormalizeHandler) Normalize(normalizers map[string]*colorizrr.Colorizrr, ctx *fasthttp.RequestCtx) {
	// unmarshal request body
	body := request{}
	err := json.Unmarshal(ctx.PostBody(), &body)
	if err != nil {
		handler.set401Response(ctx, "please provide language_code and text in correct request JSON")
		return
	}

	// validate text
	if strings.TrimSpace(body.Text) == "" {
		handler.set401Response(ctx, "text must not be empty")
		return
	}

	// validate requested language
	normalizer, exists := normalizers[body.LanguageCode]
	if !exists {
		handler.set401Response(ctx, "language code '"+body.LanguageCode+"' not supported")
		return
	}

	// and finally generate normalized colors
	colors := color.NormalizeColor(normalizer, body.Text)
	colorsJson, err := json.Marshal(colors)
	if err != nil {
		handler.set500Response(ctx, err)
		return
	}

	ctx.SetBody([]byte(`{"colors":` + string(colorsJson) + `}`))
}

func (handler *NormalizeHandler) set401Response(ctx *fasthttp.RequestCtx, message string) {
	ctx.SetStatusCode(fasthttp.StatusBadRequest)
	ctx.SetBody([]byte(`{"error":"` + message + `"}`))
}

func (handler *NormalizeHandler) set500Response(ctx *fasthttp.RequestCtx, err error) {
	ctx.SetStatusCode(fasthttp.StatusInternalServerError)
	ctx.SetBody([]byte(`{"error":"internal server error"}`))
}
