package tests

import (
	"bytes"
	"context"
	"io/ioutil"
	"net/http"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gitlab.com/mgsearch/colorizrr-server/grpc/v1/proto"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/credentials/insecure"
	"google.golang.org/grpc/status"
)

const normalizeEndpoint = "http://colorizrrapi:11111/v1/normalize"

// TestHTTP tests real HTTP call to colorizrr API
func TestHTTP(t *testing.T) {
	t.Run("test classical response", func(t *testing.T) {
		response, err := http.Post(normalizeEndpoint, "application/json", bytes.NewReader([]byte(
			`{"language_code":"cs","text":"vínová / námořnická"}`,
		)))
		if err != nil {
			t.Error(err)
		}
		defer response.Body.Close()

		body, err := ioutil.ReadAll(response.Body)
		if err != nil {
			t.Error(err)
		}
		assert.Equal(t, `{"colors":[{"id":"red","name":"červená","root":"červen","rgb":"FF0000"},{"id":"blue","name":"modrá","root":"modr","rgb":"0000FF"}]}`, string(body))
	})

	t.Run("test empty text error", func(t *testing.T) {
		response, err := http.Post(normalizeEndpoint, "application/json", bytes.NewReader([]byte(
			`{"language_code":"cs","text":" "}`,
		)))
		if err != nil {
			t.Error(err)
		}
		defer response.Body.Close()

		body, err := ioutil.ReadAll(response.Body)
		if err != nil {
			t.Error(err)
		}
		assert.Equal(t, http.StatusBadRequest, response.StatusCode)
		assert.Equal(t, `{"error":"text must not be empty"}`, string(body))
	})

	t.Run("test unknown language error", func(t *testing.T) {
		response, err := http.Post(normalizeEndpoint, "application/json", bytes.NewReader([]byte(
			`{"language_code":"unknown","text":"whatever"}`,
		)))
		if err != nil {
			t.Error(err)
		}
		defer response.Body.Close()

		body, err := ioutil.ReadAll(response.Body)
		if err != nil {
			t.Error(err)
		}
		assert.Equal(t, http.StatusBadRequest, response.StatusCode)
		assert.Equal(t, `{"error":"language code 'unknown' not supported"}`, string(body))
	})

	t.Run("test unknown endpoint error", func(t *testing.T) {
		response, err := http.Post(normalizeEndpoint+"whatever", "application/json", bytes.NewReader(nil))
		if err != nil {
			t.Error(err)
		}
		defer response.Body.Close()

		body, err := ioutil.ReadAll(response.Body)
		if err != nil {
			t.Error(err)
		}
		assert.Equal(t, http.StatusNotFound, response.StatusCode)
		assert.Equal(t, `{"error":"endpoint not found"}`, string(body))
	})

	t.Run("test swagger", func(t *testing.T) {
		response, err := http.Get("http://colorizrrapi:11111/v1/doc/index.html")
		if err != nil {
			t.Error(err)
		}
		defer response.Body.Close()

		body, err := ioutil.ReadAll(response.Body)
		if err != nil {
			t.Error(err)
		}
		assert.Equal(t, 200, response.StatusCode)
		assert.Contains(t, string(body), "Color normalization - Swagger doc") // just check for title
	})

	t.Run("test English", func(t *testing.T) {
		response, err := http.Post(normalizeEndpoint, "application/json", bytes.NewReader([]byte(
			`{"language_code":"en","text":"RED / LIME"}`,
		)))
		if err != nil {
			t.Error(err)
		}
		defer response.Body.Close()

		body, err := ioutil.ReadAll(response.Body)
		if err != nil {
			t.Error(err)
		}
		assert.Equal(t, `{"colors":[{"id":"red","name":"red","root":"red","rgb":"FF0000"},{"id":"green","name":"green","root":"green","rgb":"008000"}]}`, string(body))
	})

	t.Run("test Slovak", func(t *testing.T) {
		response, err := http.Post(normalizeEndpoint, "application/json", bytes.NewReader([]byte(
			`{"language_code":"sk","text":"žltá, dymová"}`,
		)))
		if err != nil {
			t.Error(err)
		}
		defer response.Body.Close()

		body, err := ioutil.ReadAll(response.Body)
		if err != nil {
			t.Error(err)
		}
		assert.Equal(t, `{"colors":[{"id":"yellow","name":"žltá","root":"žlt","rgb":"FFFF00"},{"id":"grey","name":"sivá","root":"siv","rgb":"808080"}]}`, string(body))
	})

	t.Run("test Slovenian", func(t *testing.T) {
		response, err := http.Post(normalizeEndpoint, "application/json", bytes.NewReader([]byte(
			`{"language_code":"si","text":"ČRNO / KHAKI"}`,
		)))
		if err != nil {
			t.Error(err)
		}
		defer response.Body.Close()

		body, err := ioutil.ReadAll(response.Body)
		if err != nil {
			t.Error(err)
		}
		assert.Equal(t, `{"colors":[{"id":"black","name":"črna","root":"črn","rgb":"000000"},{"id":"green","name":"zelena","root":"zelen","rgb":"008000"}]}`, string(body))
	})

	t.Run("test Croatian", func(t *testing.T) {
		response, err := http.Post(normalizeEndpoint, "application/json", bytes.NewReader([]byte(
			`{"language_code":"hr","text":"boja limuna / KAKI"}`,
		)))
		if err != nil {
			t.Error(err)
		}
		defer response.Body.Close()

		body, err := ioutil.ReadAll(response.Body)
		if err != nil {
			t.Error(err)
		}
		assert.Equal(t, `{"colors":[{"id":"yellow","name":"žuta","root":"žut","rgb":"FFFF00"},{"id":"green","name":"zelena","root":"zelen","rgb":"008000"}]}`, string(body))
	})
}

// TestGRPC tests real gRPC call to colorizrr API
func TestGRPC(t *testing.T) {
	connection, err := grpc.Dial("colorizrrapi:22222", grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		t.Fatal(err)
	}
	defer connection.Close()
	grpcClient := proto.NewNormalizerClient(connection)

	// test classical response
	t.Run("test classical response", func(t *testing.T) {
		ctx, cancel := context.WithTimeout(context.Background(), time.Second)
		defer cancel()
		response, err := grpcClient.Normalize(ctx, &proto.Request{
			LanguageCode: "cs",
			Text:         "vínová / námořnická",
		})
		if err != nil {
			t.Error(err)
		}
		assert.Equal(t, []*proto.Response_Colors{
			{
				Id:   "red",
				Name: "červená",
				Root: "červen",
				Rgb:  "FF0000",
			},
			{
				Id:   "blue",
				Name: "modrá",
				Root: "modr",
				Rgb:  "0000FF",
			},
		}, response.Colors)
	})

	t.Run("test empty text error", func(t *testing.T) {
		ctx, cancel := context.WithTimeout(context.Background(), time.Second)
		defer cancel()
		response, err := grpcClient.Normalize(ctx, &proto.Request{
			LanguageCode: "cs",
			Text:         "  ",
		})
		statusError, ok := status.FromError(err)
		if !ok {
			t.Error(err)
		}
		assert.Equal(t, codes.InvalidArgument, statusError.Code())
		assert.Equal(t, "text must not be empty", statusError.Message())
		assert.Nil(t, response)
	})

	t.Run("test unknown language error", func(t *testing.T) {
		ctx, cancel := context.WithTimeout(context.Background(), time.Second)
		defer cancel()
		response, err := grpcClient.Normalize(ctx, &proto.Request{
			LanguageCode: "unknown",
			Text:         "whatever",
		})
		statusError, ok := status.FromError(err)
		if !ok {
			t.Error(err)
		}
		assert.Equal(t, codes.InvalidArgument, statusError.Code())
		assert.Equal(t, "language code 'unknown' not supported", statusError.Message())
		assert.Nil(t, response)
	})
}
