Package proto contains protofile definitions and Golang generated structs used in the app. To generate / regenerate files, please follow this:

1. please install protobuf first: `sudo apt install -y protobuf-compiler`
2. go to gitlab.com/mgsearch/colorizrr-server/grpc/v1/proto
3. run e.g. `protoc --go_out=. --go_opt=paths=source_relative --go-grpc_out=. --go-grpc_opt=paths=source_relative normalize.proto` to generate files

Files must be regenerated always if contract has changed.
