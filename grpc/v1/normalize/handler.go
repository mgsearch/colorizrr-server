package normalize

import (
	"context"
	"strings"

	"github.com/rs/zerolog"
	"gitlab.com/mgsearch/colorizrr"
	"gitlab.com/mgsearch/colorizrr-server/grpc/v1/proto"
	"gitlab.com/mgsearch/colorizrr-server/service/color"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

// NewNormalizeHandler is just a constructor for NormalizeHandler
func NewNormalizeHandler(normalizers map[string]*colorizrr.Colorizrr, logger *zerolog.Logger) *NormalizeHandler {
	return &NormalizeHandler{
		normalizers: normalizers,
		logger:      logger,
	}
}

// NormalizeHandler provides simple normalization handler.
type NormalizeHandler struct {
	proto.UnimplementedNormalizerServer
	normalizers map[string]*colorizrr.Colorizrr
	logger      *zerolog.Logger
}

// Normalize processes color normalization using external normalization library and implementing gRPC interface.
func (handler *NormalizeHandler) Normalize(ctx context.Context, input *proto.Request) (*proto.Response, error) {
	handler.logger.Debug().Str("protocol", "gRPC").Str("event", "request").Str("service", "Normalizer").Str("request_data", input.String()).Send()

	// validate text
	if strings.TrimSpace(input.Text) == "" {
		err := status.Error(codes.InvalidArgument, "text must not be empty")
		handler.logger.Debug().Str("protocol", "gRPC").Str("event", "request").Str("service", "Normalizer").Err(err).Msg("validation error")
		return nil, err
	}

	// validate requested language
	normalizer, exists := handler.normalizers[input.LanguageCode]
	if !exists {
		err := status.Error(codes.InvalidArgument, "language code '"+input.LanguageCode+"' not supported")
		handler.logger.Debug().Str("protocol", "gRPC").Str("event", "request").Str("service", "Normalizer").Err(err).Msg("validation error")
		return nil, err
	}

	// and finally generate normalized colors
	colors := color.NormalizeColor(normalizer, input.Text)
	output := make([]*proto.Response_Colors, 0, len(colors))
	for _, foundColor := range colors {
		output = append(output, &proto.Response_Colors{
			Id:   foundColor.Id,
			Name: foundColor.Name,
			Root: foundColor.Root,
			Rgb:  foundColor.Rgb,
		})
	}
	response := proto.Response{Colors: output}
	handler.logger.Debug().Str("protocol", "gRPC").Str("event", "response").Str("service", "Normalizer").Str("response_data", response.String()).Send()

	return &response, nil
}
