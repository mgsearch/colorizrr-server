package server

import (
	"net"

	"github.com/rs/zerolog"
	"gitlab.com/mgsearch/colorizrr"
	"gitlab.com/mgsearch/colorizrr-server/grpc/v1/normalize"
	"gitlab.com/mgsearch/colorizrr-server/grpc/v1/proto"
	"google.golang.org/grpc"
)

// GrpcServer is just simple wrap around basic gRPC server.
type GrpcServer struct {
	address string
	grpc    *grpc.Server
}

// NewGRPCServer is a constructor for GrpcServer
func NewGRPCServer(address string, normalizers map[string]*colorizrr.Colorizrr, logger *zerolog.Logger) *GrpcServer {
	newGrpcServer := GrpcServer{
		grpc:    grpc.NewServer(),
		address: address,
	}

	// register routes - as we have 1 endpoint, no complicated system of routes required, just add this one line
	proto.RegisterNormalizerServer(newGrpcServer.grpc, normalize.NewNormalizeHandler(normalizers, logger))

	return &newGrpcServer
}

func (server *GrpcServer) Start() error {
	listener, err := net.Listen("tcp", server.address)
	if err != nil {
		return err
	}

	if err := server.grpc.Serve(listener); err != nil {
		return err
	}

	return nil
}

func (server *GrpcServer) GracefulStop() {
	server.grpc.GracefulStop()
}
