#!/bin/sh

set -e # exit immediately if a simple command exits with a nonzero exit value

GOAPP_LINT_IMAGE=colorizrrlint

# pre clean-up
docker rmi -f $GOAPP_LINT_IMAGE || true

docker build -t $GOAPP_LINT_IMAGE -f ./docker/app/lint/Dockerfile .
docker run -v `pwd`:/app -i $GOAPP_LINT_IMAGE

# post clean-up
docker rmi -f $GOAPP_LINT_IMAGE || true
