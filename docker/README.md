Package docker contains docker images of the app and also k8s konfigs you can simply (re)use if you're trying to launch your own colorizrr server on kubernetes.

**KUBERNETES KONFIGURATION**

Package k8s contains Kubernets konfigs. If you have your cluster based on Istio service mesh (https://istio.io/) with cert manager installed, you can just change domain names and node affinity and you're ready to apply as it is. If your cluster set-up differs, just take this as working source and customize it to your needs.
