#!/bin/sh

set -ex # exit immediately if a simple command exits with a nonzero exit value

API_IMAGE=colorizrrapi
API_TEST_IMAGE=colorizrrapitest
NETWORK=colorizrrtestnetwork

docker rm -f $API_TEST_IMAGE || true
docker stop $API_IMAGE || true
docker rm -f $API_IMAGE || true
docker network rm $NETWORK || true
docker rmi -f $API_TEST_IMAGE || true
docker rmi -f $API_IMAGE || true

# create network
docker network create $NETWORK

# run go app
docker build -t $API_IMAGE -f ./docker/app/Dockerfile .
docker run -d \
  --network $NETWORK \
  -p "11111:11111" \
  -p "22222:22222" \
  --name $API_IMAGE \
  $API_IMAGE

# run go tests app
set +e
docker build -t $API_TEST_IMAGE -f ./docker/app/tests/Dockerfile .
docker run -i \
  -v `pwd`:/app \
  --network $NETWORK \
  --link $API_IMAGE \
  --name $API_TEST_IMAGE \
  $API_TEST_IMAGE
RESULT=$?
set -e

# post clean-up
docker rm $API_TEST_IMAGE || true
docker stop $API_IMAGE || true
docker rm -f $API_IMAGE || true
docker network rm $NETWORK || true
docker rmi -f $API_TEST_IMAGE || true
docker rmi -f $API_IMAGE || true

exit $RESULT
